import sys
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

PAGE = """
    <!DOCTYPE html>
    <html lang="en">
      <body>
        <h1>Channel contents:</h1>
        <ul>
    {videos}
        </ul>
      </body>
    </html>
    """


class YTHandler(ContentHandler):

    def __init__(self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.link = ""
        self.videos = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')

    def endElement(self, name):
        if name == 'entry':
            self.inEntry = False
            self.videos += f"    <li><a href='{self.link}'>{self.title}</a></li>\n"
        elif self.inEntry and name == 'title':
            self.title = self.content
            self.content = ""
            self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content += chars

Parser = make_parser()
Handler = YTHandler()
Parser.setContentHandler(Handler)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python xml-parser-youtube.py <document>")
        print()
        print(" <document>: file name of the document to parse")
        sys.exit(1)

    xmlFile = open(sys.argv[1], "r")

    Parser.parse(xmlFile)
    page = PAGE.format(videos=Handler.videos)
    print(page)